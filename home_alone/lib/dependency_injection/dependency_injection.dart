import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:home_alone/dependency_injection/dio_interceptors.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/service/challenge/challenge_service.dart';
import 'package:home_alone/service/challenge/http_challenge_service.dart';
import 'package:home_alone/service/challenge_notification_service.dart';
import 'package:home_alone/service/http_category_selection_service.dart';
import 'package:home_alone/service/http_login_service.dart';
import 'package:home_alone/service/http_registration_service.dart';
import 'package:home_alone/store/category_selection_store.dart';
import 'package:home_alone/store/challenge_notification_store.dart';
import 'package:home_alone/store/challenge_store.dart';
import 'package:home_alone/store/login_store.dart';
import 'package:home_alone/store/registration_store.dart';
import 'package:home_alone/viewmodel/app_model.dart';
import 'package:home_alone/viewmodel/category_selection_model.dart';
import 'package:home_alone/viewmodel/challenges_model.dart';
import 'package:home_alone/viewmodel/login_model.dart';
import 'package:home_alone/viewmodel/registration_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DependencyInjection {
  static Future<void> setUp() async {
    await DotEnv().load('.env');
    await _setUpServices();
    await _setUpViewModels();
    await _setUpStores();
  }

  static Future<void> logout() async {
    print("performing logout");
    locator.get<FlutterSecureStorage>().delete(key: "token");
    locator.get<FlutterSecureStorage>().delete(key: "user");
    locator.reset();
    (await SharedPreferences.getInstance()).clear();

    await setUp();
  }

  static void loadChallengesIfLoggedIn() {
    locator.get<ChallengeStore>().loadSubscribedChallenges();
  }

  static Future<void> _setUpServices() async {
    var dio = Dio(BaseOptions(
      receiveDataWhenStatusError: true,
      connectTimeout: 5000,
      receiveTimeout: 5000,
    ));

    dio = LoginInterceptor.addInterceptors(dio);

    locator.registerSingleton(ChallengeNotificationService());

    locator.registerSingleton<ChallengeService>(HttpChallengeService(
      baseUrl: DotEnv().env['BASE_URL'],
      dio: dio,
    ));

    var storage = new FlutterSecureStorage();
    await LoginInterceptor.readTokenFromStore(storage);
    locator.registerSingleton(storage);

    locator.registerSingleton<HttpRegistrationService>(HttpRegistrationService(
      dio: dio,
      baseUrl: DotEnv().env['BASE_URL'],
    ));
    locator.registerSingleton<HttpLoginService>(HttpLoginService(
      dio: dio,
      baseUrl: DotEnv().env['BASE_URL'],
    ));
    locator.registerSingleton<HttpCategorySelectionService>(
        HttpCategorySelectionService(
      dio: dio,
      baseUrl: DotEnv().env['BASE_URL'],
    ));
  }

  static Future<void> _setUpViewModels() async {
    locator.registerSingleton(AppModel());
    locator.registerSingleton(RegistrationModel());
    locator.registerSingleton(LoginModel());
    locator.registerSingleton(CategorySelectionModel());
    locator.registerSingleton(ChallengesModel());
  }

  static Future<void> _setUpStores() async {
    locator.registerSingleton(LoginStore(
      locator.get<LoginModel>(),
      locator.get<HttpLoginService>(),
    ));
    locator.registerSingleton(RegistrationStore(
      locator.get<RegistrationModel>(),
      locator.get<HttpRegistrationService>(),
    ));
    locator.registerSingleton(CategorySelectionStore(
      locator.get<CategorySelectionModel>(),
      locator.get<HttpCategorySelectionService>(),
    ));
    locator.registerSingleton(ChallengeStore(
      locator.get<ChallengeService>(),
      locator.get<ChallengesModel>(),
    ));
    locator.registerSingleton(ChallengeNotificationStore(
      locator.get<ChallengeNotificationService>(),
    ));
  }
}
