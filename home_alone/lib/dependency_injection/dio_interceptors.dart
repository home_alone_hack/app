import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/model/user.dart';

class LoginInterceptor {
  static String token;

  static dynamic requestInterceptor(RequestOptions options) {
    if (token == null) return options;
    options.headers.addAll({"Authorization": "Bearer " + token});
    return options;
  }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(InterceptorsWrapper(
          onResponse: (Response response) => responseInterceptor(response),
          onRequest: (RequestOptions options) => requestInterceptor(options),
          onError: (DioError dioError) => dioError));
  }

  static dynamic responseInterceptor(Response options) async {
    if (options.request.method != 'POST') return options;
    if (options.statusCode < 299) {
      if ((options.request.path.endsWith("/auth/login") ||
          options.request.path.endsWith("/auth/register"))) {
        var data = options.data as Map<String, dynamic>;

        if (data['accessToken'] == null) return options;
        token = data['accessToken'];
        locator.get<FlutterSecureStorage>().write(key: "token", value: token);

        if (data['user'] == null) return options;
        User user = User.fromJson(data['user']);
        locator
            .get<FlutterSecureStorage>()
            .write(key: "user", value: jsonEncode(user.toJson()));
      }
    }
    return options;
  }

  static Future<void> readTokenFromStore(FlutterSecureStorage storage) async {
    token = await storage.read(key: "token");
  }
}
