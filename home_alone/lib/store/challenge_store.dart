import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/service/challenge/challenge_service.dart';
import 'package:home_alone/viewmodel/challenge_model.dart';
import 'package:home_alone/viewmodel/challenges_model.dart';

class ChallengeStore {
  final ChallengeService service;
  final ChallengesModel challengesModel;

  ChallengeStore(
    this.service,
    this.challengesModel,
  );

  Future<void> loadSubscribedChallenges() async {
    challengesModel.isLoading = true;
    final challenges = await service.getSubscribedChallenges()
      ..where((f) => f.accepted == null);
    print(challenges);
    challengesModel.subScribedChallenges = challenges._toModelView();
  }

  List<ChallengeModel> findChallenges(String query) {
    return challengesModel.subScribedChallenges
            .where((x) =>
                x.challenge.name.toLowerCase().contains(query.toLowerCase()))
            .toList() ??
        [];
  }

  Future<void> acceptChallenge(String id) async {
    final isAccepted = await service.acceptChallenge(id);
    if (isAccepted) {
      final model = challengesModel.subScribedChallenges
          ?.firstWhere((m) => m.challenge.id == id, orElse: null);
      model.challenge = model.challenge..finished = false;
    }
  }

  Future<bool> finishChallenge(String challengeId) =>
      service.finishChallenge(challengeId);
}

extension on List<Challenge> {
  List<ChallengeModel> _toModelView() =>
      this.map((f) => ChallengeModel(f)).toList();
}
