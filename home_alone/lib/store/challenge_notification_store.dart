import 'dart:math';

import 'package:home_alone/model/challenge_notification.dart';
import 'package:home_alone/service/challenge_notification_service.dart';

class ChallengeNotificationStore {
  final ChallengeNotificationService service;

  ChallengeNotificationStore(this.service);

  Future<void> setNotificationEnabled(
    String challengeId,
    bool isEnabled,
  ) async {
    var notification = await service.getNotification(challengeId);
    if (notification == null || notification.challengeId == null) {
      print("no notification Stored");
      notification = ChallengeNotification(
        challengeId: challengeId,
        notificationsEnabled: isEnabled,
        notificationsPerDay: _notificationsPerDayMock(),
      );
    } else {
      notification = ChallengeNotification(
        challengeId: notification.challengeId,
        notificationsEnabled: isEnabled,
        notificationsPerDay: notification.notificationsPerDay,
      );
    }
    await service.storeNotification(notification);
  }

  double _notificationsPerDayMock() {
    return Random().nextDouble() * 10;
  }
}
