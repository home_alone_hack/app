import 'package:flutter/material.dart';
import 'package:home_alone/view/theme/border.dart';
import 'package:home_alone/view/theme/icons.dart';
import 'package:home_alone/viewmodel/app_model.dart';
import 'package:provider/provider.dart';

class ThemedAppBar extends AppBar {
  ThemedAppBar({
    String title,
    actions,
    showLogo = false,
  }) : super(
          centerTitle: true,
          shape: CurvedAppBarBorder(),
          title: _buildTitleText(title),
          actions: actions,
          elevation: 0.0,
          leading:
              showLogo ? Icon(HomeAloneIcons.home_alone_logo, size: 24) : null,
        );

  static Widget _buildTitleText(String title) {
    return Builder(
      builder: (context) => Text(
        title ?? Provider.of<AppModel>(context).tab.name,
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}
