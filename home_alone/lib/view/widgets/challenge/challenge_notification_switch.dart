import 'package:flutter/material.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/model/challenge_notification.dart';
import 'package:home_alone/store/challenge_notification_store.dart';

class ChallengeNotificationSwitch extends StatefulWidget {
  final Challenge challenge;

  const ChallengeNotificationSwitch({this.challenge});

  @override
  _ChallengeNotificationSwitchState createState() =>
      _ChallengeNotificationSwitchState();
}

class _ChallengeNotificationSwitchState
    extends State<ChallengeNotificationSwitch> {
  @override
  Widget build(BuildContext context) => FutureBuilder(
        future: widget.challenge.notification,
        builder: (context, AsyncSnapshot<ChallengeNotification> snapshot) {
          final isEnabled =
              snapshot.hasData && snapshot.data.notificationsEnabled;
          return InkWell(
            onTap: () => _setNotifcationEnabled(isEnabled),
            child: Icon(
              isEnabled ? Icons.notifications_active : Icons.notifications_off,
              color: isEnabled ? Colors.amber : Colors.white,
              size: 42,
            ),
          );
        },
      );

  void _setNotifcationEnabled(bool isEnabled) {
    locator
        .get<ChallengeNotificationStore>()
        .setNotificationEnabled(widget.challenge.id, !isEnabled);
    setState(() {});
  }
}
