import 'package:flutter/material.dart';

import 'package:home_alone/model/category.dart';
import 'package:home_alone/view/widgets/challenge/challenge_icon.dart';
import 'package:home_alone/view/widgets/themed_text.dart';

class ChallengeDetailCategoryIconLabel extends StatelessWidget {
  const ChallengeDetailCategoryIconLabel({
    this.category,
    this.size = 25,
  });
  final Category category;

  final double size;
  double get fontSize => size * 0.9;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          child: CategoryIcon(
            maxRadius: size,
            category: category,
          ),
          padding: EdgeInsets.only(left: 4.0, top: 8.0, bottom: 8.0),
        ),
        Padding(
          child: ThemedText(
            fontSize: fontSize,
            text: category.name,
            color: Color(0xFF00AB96),
            textAlign: TextAlign.left,
          ),
          padding: EdgeInsets.only(left: 4, right: 8),
        )
      ],
    );
  }
}
