import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:home_alone/view/widgets/caption_text.dart';
import 'package:home_alone/view/widgets/challenge/discover_challenge_tile.dart';
import 'package:home_alone/viewmodel/challenge_model.dart';

class ChallengesList extends StatelessWidget {
  const ChallengesList({
    this.challenges,
    this.itemWidth,
    this.itemHeight,
    this.caption,
    this.placeHolderText,
    this.scrollDirection = Axis.vertical,
    Key key,
  }) : super(key: key);

  final List<ChallengeModel> challenges;
  final double itemWidth;
  final double itemHeight;
  final String caption;
  final String placeHolderText;
  final Axis scrollDirection;
  // final Key key;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CaptionText(caption),
        challenges.isEmpty ? Text(placeHolderText) : _buildList(),
      ],
    );
  }

  Widget _buildList() {
    return SizedBox(
      height: itemHeight,
      child: ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        scrollDirection: scrollDirection,
        children: _buildListItems(),
      ),
    );
  }

  List<Widget> _buildListItems() => challenges
      .map(
        (f) => DiscoverChallengeTile(
          f,
          () {},
          fromAcceptedChallenges: true,
          width: itemWidth,
        ),
      )
      .toList();
}
