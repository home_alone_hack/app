import 'package:flutter/material.dart';
import 'package:share/share.dart';

import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/view/widgets/themed_flat_button.dart';

class ShareChallengeButton extends StatelessWidget {
  const ShareChallengeButton({@required this.challenge});

  final Challenge challenge;

  @override
  Widget build(BuildContext context) {
    return ThemedFlatButton(
        text: 'Share',
        onPressed: () {
          Share.share(
              'Spiel mit mir die ${challenge.name}. In der home alone challenge App.');
        });
  }
}
