import 'package:flutter/material.dart';
import 'package:home_alone/dependency_injection/dependency_injection.dart';

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Image.asset("assets/image/log_out.png"),
      onPressed: () => _logout(context),
    );
  }

  _logout(BuildContext context) async {
    await DependencyInjection.logout();
    Navigator.of(context).pushNamedAndRemoveUntil("/login", (route) => false);
  }
}
