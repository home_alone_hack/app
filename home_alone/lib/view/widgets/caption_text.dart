import 'package:flutter/material.dart';
import 'package:home_alone/view/widgets/themed_text.dart';

class CaptionText extends StatelessWidget {
  const CaptionText(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8.0, top: 16.0),
      child: Container(
        width: double.infinity,
        child: ThemedText(
          text: text,
          color: Color(0xFF00AB96),
          textAlign: TextAlign.start,
          fontSize: 16.0,
        ),
      ),
    );
  }
}
