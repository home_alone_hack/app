import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/model/user.dart';
import 'package:home_alone/view/theme/dime.dart';
import 'package:home_alone/view/widgets/themed_text.dart';

class UserName extends StatelessWidget {
  @override
  Widget build(BuildContext context) => FutureBuilder(
        future: _getUserName(),
        builder: _buildContent,
      );

  Future<String> _getUserName() async {
    var storage = locator.get<FlutterSecureStorage>();
    var value = await storage.read(key: "user");
    if (value == null) {
      return null;
    }
    final user = User.fromJson(jsonDecode(value));
    return user.displayedName;
  }

  Widget _buildContent(
    BuildContext context,
    AsyncSnapshot<String> userDisplayName,
  ) =>
      userDisplayName.hasData
          ? ThemedText(
              text: 'Angemeldet als: ${userDisplayName.data}',
              fontSize: HomeAloneDimensions.labelFontSize,
            )
          : Container();
}
