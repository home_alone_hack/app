import 'package:flutter/material.dart';
import 'package:home_alone/service/challenge_search_delegate.dart';
import 'package:home_alone/view/theme/icons.dart';

class SearchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) => IconButton(
        icon: Icon(HomeAloneIcons.search, size: 20),
        onPressed: () {
          showSearch(
            context: context,
            delegate: ChallengeSearchDelegate(
              keyboardType: TextInputType.text,
              searchFieldLabel: "Name der Challenge ...",
            ),
          );
        },
      );
}
