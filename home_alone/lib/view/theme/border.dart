import 'package:flutter/material.dart';

class CurvedAppBarBorder extends ContinuousRectangleBorder {
  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    final size = rect.size;
    return Path()
      ..lineTo(0, size.height)
      ..quadraticBezierTo(
        size.width / 2,
        size.height + 8,
        size.width,
        size.height,
      )
      ..lineTo(size.width, 0)
      ..close();
  }
}
