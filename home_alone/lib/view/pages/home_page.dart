import 'package:flutter/material.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/view/pages/challenge_list_page.dart';
import 'package:home_alone/view/pages/dashboard_page.dart';
import 'package:home_alone/view/pages/settings_page.dart';
import 'package:home_alone/view/theme/colors.dart';
import 'package:home_alone/view/widgets/logout_button.dart';
import 'package:home_alone/view/widgets/search_button.dart';
import 'package:home_alone/view/widgets/themed_app_bar.dart';
import 'package:home_alone/viewmodel/app_model.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController =
        TabController(vsync: this, length: _navigationItems.length);
    _tabController.addListener(_onIndexChanged);
    super.initState();
  }

  void _onIndexChanged() {
    setState(() {
      if (!_tabController.indexIsChanging) {
        Provider.of<AppModel>(context, listen: false).tab =
            Tabs.values[_tabController.index];
      }
    });
  }

  bool get isOnSettingsTab => locator.get<AppModel>().tab == Tabs.settings;

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: _buildAppBar(context),
        body: _buildContent(context),
        bottomNavigationBar: _buildTabBar(context),
      );

  AppBar _buildAppBar(BuildContext context) => ThemedAppBar(
        showLogo: true,
        actions: [isOnSettingsTab ? LogoutButton() : SearchButton()],
      );

  Widget _buildContent(BuildContext context) => TabBarView(
        controller: _tabController,
        children: [
          DashboardPage(key: PageStorageKey('dashboard')),
          ChallengeListPage(key: PageStorageKey('challenges')),
          SettingsPage(),
        ],
      );

  Widget _buildTabBar(BuildContext context) => Container(
        color: Theme.of(context).primaryColor,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: TabBar(
          controller: _tabController,
          indicator: ShapeDecoration(
              shape: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 0,
                      style: BorderStyle.solid)),
              gradient: LinearGradient(
                colors: [
                  HomeAloneColors.primaryButtonGradientStartColor,
                  HomeAloneColors.primaryButtonGradientEndColor,
                ],
              )),
          unselectedLabelColor: Colors.white,
          labelColor: Theme.of(context).hintColor,
          tabs: _navigationItems,
        ),
      );

  final _navigationItems = <Widget>[
    Tab(icon: buildTabIcon("home"), text: "Entdecken"),
    Tab(icon: buildTabIcon("challenges"), text: "Meine Challenges"),
    Tab(icon: buildTabIcon("settings"), text: "Einstellungen"),
  ];

  static Widget buildTabIcon(String imageName) =>
      Image.asset("assets/image/$imageName.png", width: 28, height: 28);
}
