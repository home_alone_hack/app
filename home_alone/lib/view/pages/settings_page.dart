import 'package:flutter/material.dart';

import 'package:home_alone/view/pages/category_selection_page.dart';
import 'package:home_alone/view/widgets/settings/user_name.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Center(
          child: Column(
            children: [
              UserName(),
              _buildCategorySelection(),
            ],
          ),
        ),
      );

  _buildCategorySelection() => Expanded(
        child: CategorySelection(
          fromSettings: true,
          buildScaffold: false,
        ),
      );
}
