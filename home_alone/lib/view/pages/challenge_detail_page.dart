import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:home_alone/view/theme/dime.dart';
import 'package:home_alone/view/widgets/challenge/challenge_detail_category_icon_label.dart';
import 'package:home_alone/view/widgets/challenge/challenge_notification_switch.dart';
import 'package:home_alone/view/widgets/challenge/share_challenge_button.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/model/category.dart';
import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/store/challenge_store.dart';
import 'package:home_alone/view/pages/login_page.dart';
import 'package:home_alone/view/widgets/label_text.dart';
import 'package:home_alone/view/widgets/themed_app_bar.dart';
import 'package:home_alone/view/widgets/themed_button.dart';
import 'package:home_alone/view/widgets/themed_text.dart';
import 'package:home_alone/view/widgets/weird/weird_ball.dart';
import 'package:home_alone/viewmodel/challenge_model.dart';

const URLS = [
  "https://www.stendo.net/mobile/img/senior/senior-13.jpg",
  "https://www.meridianspa.de/fileadmin/user_upload/Fitness-Hamburg-Meridian-Plank.jpg"
];

class ChallengeDetail extends StatelessWidget with AwfulKeyboardMixin {
  final ChallengeModel challenge;

  ChallengeDetail({this.challenge});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ThemedAppBar(
        title: challenge.challenge.name,
        actions: [_buildNotificationButton()],
      ),
      body: _buildBody(context).withAwfulKeyboardFix(this),
    );
  }

  Widget _buildNotificationButton() => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: ChallengeNotificationSwitch(challenge: challenge.challenge),
      );

  Widget _buildBody(BuildContext context) {
    return SafeArea(
        child: ChangeNotifierProvider.value(
      value: challenge,
      child: Consumer<ChallengeModel>(
        builder: (context, model, _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(child: _buildContent(context)),
              _buildButtonOrSlider(context),
            ],
          );
        },
      ),
    ));
  }

  Widget _buildContent(BuildContext context) => ListView(
        shrinkWrap: true,
        children: <Widget>[
          _buildImageSection(context),
          _buildText(challenge.challenge),
        ],
      );

  Widget _buildImageSection(BuildContext context) => Stack(children: <Widget>[
        _buildImageAndShareButton(context),
        Positioned(
          top: 180,
          child: ChallengeDetailCategoryIconLabel(
            category: challenge.challenge.category,
          ),
        ),
      ]);

  Column _buildImageAndShareButton(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        _buildImage(challenge.challenge, context),
        ShareChallengeButton(challenge: challenge.challenge)
      ],
    );
  }

  Widget _buildImage(Challenge challenge, BuildContext context) {
    return Container(
        constraints: BoxConstraints.expand(
          height: HomeAloneDimensions.challengeImageHeight,
        ),
        // placeholder if image load fails
        decoration: BoxDecoration(color: Theme.of(context).accentColor),
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: CachedNetworkImage(
              imageUrl: challenge.imageUrl,
              placeholder: (context, url) => Center(
                    child: Container(
                        width: 36,
                        height: 36,
                        child: CircularProgressIndicator()),
                  )),
        ));
  }

  Widget _buildText(Challenge challenge) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(children: [
        SizedBox(height: 10),
        ThemedText(text: challenge.name),
        LabelText(
          text: challenge.description,
          textAlign: TextAlign.start,
        ),
      ]),
    );
  }

  Widget _buildButtonOrSlider(BuildContext context) {
    if (challenge?.challenge?.finished == true) {
      return Container();
    }
    if (challenge.challenge.accepted == true) {
      return _buildSlider(context);
    }
    return _buildAcceptChallengeButton();
  }

  Widget _buildSlider(BuildContext context) => Padding(
        child: ThemedButton(
          text: 'Challenge abschießen',
          onPressed: () => _finishChallenge(context),
        ),
        padding: EdgeInsets.all(16.0),
      );

  Widget _buildAcceptChallengeButton() => Padding(
        child: ThemedButton(
          text: 'Teilnehmen',
          onPressed: _acceptChallenge,
        ),
        padding: EdgeInsets.all(16.0),
      );

  void _acceptChallenge() {
    locator.get<ChallengeStore>().acceptChallenge(challenge.challenge.id);
  }

  Widget _buildFinishedPopup(BuildContext context) {
    return AlertDialog(
        contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        content: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Image.asset(
                'assets/challenge_accomplished_face.png',
                width: 200.0,
                fit: BoxFit.cover,
              ),
              Padding(
                child: ThemedText(
                  fontSize: 22.0,
                  text: 'Super du hast die Challenge erledigt!',
                ),
                padding: EdgeInsets.only(top: 8, bottom: 8),
              ),
              Text(
                'Du hast die Challenge ${challenge.challenge.name} abgeschlossen.',
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 8),
                child: ThemedButton(
                  text: 'Weiter',
                  onPressed: () {
                    Navigator.pushNamed(context, '/home');
                  },
                ),
              )
            ],
          ),
        ));
  }

  _finishChallenge(BuildContext context) async {
    await locator.get<ChallengeStore>().finishChallenge(challenge.challenge.id);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return _buildFinishedPopup(context);
        });
  }
}

extension on Widget {
  Widget withCategoryFooter(Category category) => Stack(
        children: <Widget>[
          this,
          Positioned(
            top: 170,
            child: ChallengeDetailCategoryIconLabel(category: category),
          ),
        ],
      );
}
