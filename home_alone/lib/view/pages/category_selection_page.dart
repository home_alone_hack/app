import 'package:flutter/material.dart';
import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/store/category_selection_store.dart';
import 'package:home_alone/view/theme/dime.dart';
import 'package:home_alone/view/widgets/categories/category_list.dart';
import 'package:home_alone/view/widgets/themed_app_bar.dart';
import 'package:home_alone/view/widgets/themed_button.dart';
import 'package:home_alone/view/widgets/themed_text.dart';
import 'package:home_alone/viewmodel/category_selection_model.dart';
import 'package:provider/provider.dart';

class CategorySelection extends StatefulWidget {
  final bool fromSettings;
  final bool buildScaffold;
  CategorySelection({
    @required this.fromSettings,
    this.buildScaffold = true,
  });

  @override
  _CategorySelectionState createState() => _CategorySelectionState();
}

class _CategorySelectionState extends State<CategorySelection> {
  @override
  void initState() {
    locator.get<CategorySelectionStore>().loadCategories();
    locator.get<CategorySelectionStore>().loadSelectedCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => widget.buildScaffold
      ? Scaffold(
          appBar: ThemedAppBar(
            title: 'Kategorien',
          ),
          body: _buildBody(context),
        )
      : _buildBody(context);

  Widget _buildBody(BuildContext context) {
    return SafeArea(
        child: Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 0),
      child: Center(
        child: ChangeNotifierProvider.value(
          value: locator.get<CategorySelectionModel>(),
          child: Builder(builder: (context) => _buildContent(context)),
        ),
      ),
    ));
  }

  Column _buildContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 8),
        _buildTextAndList(context),
        if (!widget.fromSettings) _buildNextButton(context),
      ],
    );
  }

  _buildTextAndList(BuildContext context) => Expanded(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildLabelText(),
              SizedBox(height: 8),
              Flexible(child: _buildCategoryList(context)),
            ],
          ),
        ),
      );

  Widget _buildCategoryList(BuildContext context) =>
      CategoryList(widget.fromSettings);

  Widget _buildLabelText() => ThemedText(
        text: 'Wofür interessierst du dich?',
        fontSize: HomeAloneDimensions.welcomeHeaderSmallerTextSize,
      );

  Widget _buildNextButton(BuildContext context) {
    var actionButtonIsEnabled =
        Provider.of<CategorySelectionModel>(context).actionButtonIsEnabled;
    return ThemedButton(
      text: 'Weiter',
      onPressed: actionButtonIsEnabled ? _goToHomePage : null,
    );
  }

  Future<void> _goToHomePage() async {
    await _sendCategorySelectionOpenHomePage(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
  }

  _sendCategorySelectionOpenHomePage(BuildContext context) async {
    await locator.get<CategorySelectionStore>().updateCategories();
  }
}
