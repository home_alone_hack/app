import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/store/challenge_store.dart';
import 'package:home_alone/view/widgets/challenge/discover_challenge_tile.dart';
import 'package:home_alone/view/widgets/challenge/challenge_icon.dart';
import 'package:home_alone/view/widgets/shimmer_effect.dart';
import 'package:home_alone/view/widgets/themed_text.dart';
import 'package:home_alone/viewmodel/challenges_model.dart';

class DashboardPage extends StatelessWidget {
  DashboardPage({Key key}) : super(key: key);

  final RefreshController refreshController = RefreshController();

  void _onRefresh() async {
    await locator.get<ChallengeStore>().loadSubscribedChallenges();
    refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: refreshController,
      onRefresh: _onRefresh,
      child: ChangeNotifierProvider.value(
        value: locator.get<ChallengesModel>(),
        child: Builder(builder: (context) => _buildContent(context)),
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    final model = Provider.of<ChallengesModel>(context);
    return model.isLoading ? ShimmerEffect() : buildChallengeLists(model);
  }

  Widget buildChallengeLists(ChallengesModel challenges) {
    return SingleChildScrollView(
        key: PageStorageKey('dashboardChallenges'),
        child: Padding(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: _buildChallengeItems(challenges),
          ),
          padding: EdgeInsets.all(8.0),
        ));
  }

  List<Widget> _buildChallengeItems(ChallengesModel challenges) {
    return challenges.categories.entries.map((m) {
      final category = m.key;
      final challenges = m.value;
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  child: CategoryIcon(maxRadius: 16, category: category),
                  padding: EdgeInsets.only(left: 4.0),
                ),
                Padding(
                  child: ThemedText(
                    fontSize: 16,
                    text: category.name,
                    textAlign: TextAlign.left,
                  ),
                  padding: EdgeInsets.only(left: 8),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 4),
            child: SizedBox(
              height: 160,
              child: ListView.builder(
                key: PageStorageKey(m.key.name),
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: challenges.length,
                itemBuilder: (BuildContext context, int index) =>
                    DiscoverChallengeTile(
                        challenges[index], () => _onRefresh()),
              ),
            ),
          )
        ],
      );
    }).toList();
  }
}
