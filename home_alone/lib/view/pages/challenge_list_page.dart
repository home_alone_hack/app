import 'package:flutter/material.dart';
import 'package:home_alone/view/widgets/challenge/list/accepted_challenges_list.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/store/challenge_store.dart';
import 'package:home_alone/view/widgets/shimmer_effect.dart';
import 'package:home_alone/viewmodel/challenges_model.dart';

class ChallengeListPage extends StatelessWidget {
  ChallengeListPage({Key key}) : super(key: key);

  final RefreshController refreshController = RefreshController();

  void _onRefresh() async {
    await locator.get<ChallengeStore>().loadSubscribedChallenges();
    refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: refreshController,
      onRefresh: _onRefresh,
      child: ChangeNotifierProvider.value(
        value: locator.get<ChallengesModel>(),
        child: Builder(builder: (context) => _buildContent(context)),
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    final model = Provider.of<ChallengesModel>(context);
    return model.isLoading ? ShimmerEffect() : _buildLists(model);
  }

  ListView _buildLists(ChallengesModel model) {
    return ListView(
      key: PageStorageKey('challengesPageInnerList'),
      children: <Widget>[
        _buildFinishedChallengesList(model),
        _buildAcceptedChallengesList(model),
      ],
    );
  }

  Widget _buildFinishedChallengesList(ChallengesModel model) => ChallengesList(
        challenges: model.finishedChallenges,
        caption: 'Geschafft!',
        placeHolderText: 'Noch keine Challenges abgeschlossen',
        itemWidth: 250,
        itemHeight: 160,
        scrollDirection: Axis.horizontal,
        key: PageStorageKey('finishedChallenges'),
      );

  Widget _buildAcceptedChallengesList(ChallengesModel model) => ChallengesList(
        challenges: model.acceptedChallenges,
        caption: 'Offene Challenges',
        placeHolderText: 'Noch keine Challenges begonnen',
        key: PageStorageKey('acceptedChallenges'),
      );
}
