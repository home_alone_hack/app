import 'package:flutter/material.dart';

enum Tabs { discover, challenges, settings }

extension TabNames on Tabs {
  static const tabNames = {
    Tabs.discover: "Entdecken",
    Tabs.challenges: "Meine Challenges",
    Tabs.settings: "Einstellungen",
  };

  String get name => tabNames[this];
}

class AppModel extends ChangeNotifier {
  Tabs _tab = Tabs.discover;
  Tabs get tab => _tab;
  set tab(Tabs newTab) {
    _tab = newTab;
    notifyListeners();
  }
}
