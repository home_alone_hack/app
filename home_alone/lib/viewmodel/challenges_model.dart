import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:home_alone/model/category.dart';
import 'package:home_alone/viewmodel/challenge_model.dart';

class ChallengesModel extends ChangeNotifier {
  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  List<ChallengeModel> _subScribedChallenges = List<ChallengeModel>();

  set subScribedChallenges(List<ChallengeModel> newChallenges) {
    _subScribedChallenges = newChallenges;
    newChallenges?.forEach((f) => f.addListener(_notifyListeners));
    _isLoading = false;
    notifyListeners();
  }

  List<ChallengeModel> get subScribedChallenges => _subScribedChallenges;
  List<ChallengeModel> get subScribedUnfinishedChallenges =>
      _subScribedChallenges.where((c) => c.challenge.finished == null).toList();
  List<ChallengeModel> get acceptedChallenges => _subScribedChallenges
      .where((t) => t.challenge.finished == false)
      .toList();
  List<ChallengeModel> get finishedChallenges =>
      _subScribedChallenges.where((t) => t.challenge.finished == true).toList()
        ..sort(_sortAcceptedDate);

  void _notifyListeners() => notifyListeners();

  Map<Category, List<ChallengeModel>> get categories =>
      groupBy<ChallengeModel, Category>(
          subScribedUnfinishedChallenges, (v) => v.challenge.category);

  int _sortAcceptedDate(ChallengeModel a, ChallengeModel b) {
    {
      final milA = a.challenge.acceptedAt.millisecondsSinceEpoch;
      final milB = b.challenge.acceptedAt.millisecondsSinceEpoch;
      if (milA < milB) {
        return 1;
      } else {
        return -1;
      }
    }
  }
}
