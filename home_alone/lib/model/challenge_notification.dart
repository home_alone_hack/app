import 'package:json_annotation/json_annotation.dart';

part 'challenge_notification.g.dart';

@JsonSerializable(nullable: true)
class ChallengeNotification {
  ChallengeNotification({
    this.notificationsPerDay,
    this.notificationsEnabled,
    this.challengeId,
  });

  final double notificationsPerDay;
  final bool notificationsEnabled;
  final String challengeId;

  factory ChallengeNotification.fromJson(Map<String, dynamic> json) =>
      _$ChallengeNotificationFromJson(json);

  Map<String, dynamic> toJson() => _$ChallengeNotificationToJson(this);
}
