// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChallengeNotification _$ChallengeNotificationFromJson(
    Map<String, dynamic> json) {
  return ChallengeNotification(
    notificationsPerDay: (json['notificationsPerDay'] as num)?.toDouble(),
    notificationsEnabled: json['notificationsEnabled'] as bool,
    challengeId: json['challengeId'] as String,
  );
}

Map<String, dynamic> _$ChallengeNotificationToJson(
        ChallengeNotification instance) =>
    <String, dynamic>{
      'notificationsPerDay': instance.notificationsPerDay,
      'notificationsEnabled': instance.notificationsEnabled,
      'challengeId': instance.challengeId,
    };
