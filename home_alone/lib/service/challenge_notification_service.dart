import 'dart:convert';

import 'package:home_alone/model/challenge_notification.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChallengeNotificationService {
  Future<SharedPreferences> get sharedPreferences =>
      SharedPreferences.getInstance();

  Future<void> storeNotification(ChallengeNotification notification) async {
    final sp = await sharedPreferences;
    await sp.setString(
      notification._toSharedPrefsKey(),
      jsonEncode(notification.toJson()),
    );
    _verify(notification);
  }

  Future<ChallengeNotification> getNotification(String challengeId) async {
    final sp = await sharedPreferences;
    final notificationAsString = sp.getString("notification.$challengeId");
    if (notificationAsString?.isNotEmpty != true) {
      return null;
    }

    final notificationJson = jsonDecode(notificationAsString);
    final notification = ChallengeNotification.fromJson(notificationJson);

    return notification;
  }

  void _verify(ChallengeNotification notification) async {
    final notificationStringFromStore =
        await getNotification(notification.challengeId);
    assert(notificationStringFromStore.challengeId == notification.challengeId);
    assert(notificationStringFromStore.notificationsEnabled ==
        notification.notificationsEnabled);
    assert(notificationStringFromStore.notificationsPerDay ==
        notification.notificationsPerDay);
    print("Notification stored correctly");
  }
}

extension on ChallengeNotification {
  String _toSharedPrefsKey() => "notification.$challengeId";
}
