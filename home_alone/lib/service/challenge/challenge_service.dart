import 'package:home_alone/model/challenge.dart';

abstract class ChallengeService {
  Future<List<Challenge>> getAllChallenges();
  Future<List<Challenge>> getSubscribedChallenges();
  Future<List<Challenge>> getAcceptedChallenges();
  Future<List<Challenge>> findChallenges(String query);
  Future<bool> finishChallenge(String challengeId);
  Future<bool> acceptChallenge(String id);
}
