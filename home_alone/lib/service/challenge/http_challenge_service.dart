import 'package:dio/dio.dart';
import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/service/challenge/challenge_service.dart';
import 'package:home_alone/viewmodel/challenges_model.dart';

typedef ResultMapper<T> = T Function(dynamic data);
enum ResponseType { NOT_AUTHENTICATED, FORBIDDEN, SERVER_ERROR }

class HttpChallengeService implements ChallengeService {
  String baseUrl;
  Dio dio;

  Map<String, dynamic> defaultQueryParams;

  HttpChallengeService({
    this.baseUrl,
    this.dio,
  }) : defaultQueryParams = {};

  ChallengesModel get challengesModel => locator.get<ChallengesModel>();

  @Deprecated("No need for this method")
  @override
  Future<List<Challenge>> getAllChallenges() async {
    var response = await dio.get("$baseUrl/challenge");
    return Challenge.fromJsonList(response.data);
  }

  @override
  Future<List<Challenge>> getSubscribedChallenges() async {
    var response = await dio.get("$baseUrl/user/challenge");
    return Challenge.fromJsonList(response.data);
  }

  @Deprecated("Use ChallengeModel.acceptedChallenges")
  @override
  Future<List<Challenge>> getAcceptedChallenges() async {
    var response = await dio.get("$baseUrl/user/challenge/accepted");
    return Challenge.fromJsonList(response.data);
  }

  @Deprecated("Searching the ViewModel is quite fast... for now")
  @override
  Future<List<Challenge>> findChallenges(String query) async {
    final response = await dio
        .get("$baseUrl/challenge/search", queryParameters: {'q': query});

    try {
      if (response.statusCode == 200) {
        return Challenge.fromJsonList(response.data ?? []);
      } else {
        return [];
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  @override
  Future<bool> acceptChallenge(String id) async {
    var response = await dio.post("$baseUrl/challenge/$id/accept");
    print(response.statusCode);
    print(response.data);
    if (response.statusCode < 299) {
      getSubscribedChallenges();
      return true;
    }
    return false;
  }

  @override
  Future<bool> finishChallenge(String challengeId) async {
    try {
      await dio.put("$baseUrl/challenge/$challengeId/finish");
      return true;
    } catch (err) {
      print(err);
      return false;
    }
  }
}
