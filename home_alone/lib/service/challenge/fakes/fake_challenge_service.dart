import 'dart:convert';

import 'package:home_alone/model/challenge.dart';
import 'package:home_alone/service/challenge/challenge_service.dart';

import 'challenge_data.dart';

class FakeChallengeService implements ChallengeService {
  @override
  Future<List<Challenge>> getAllChallenges() async {
    return Challenge.fromJsonList(jsonDecode(all));
  }

  @override
  Future<List<Challenge>> getSubscribedChallenges() async {
    return Challenge.fromJsonList(jsonDecode(all));
  }

  @override
  Future<List<Challenge>> getAcceptedChallenges() async {
    return Challenge.fromJsonList(jsonDecode(accepted));
  }

  @override
  Future<List<Challenge>> findChallenges(String query) async {
    final challenges = await getAllChallenges();
    var result = challenges
        .where((challenge) => challenge.name.toLowerCase().startsWith(query))
        .toList();
    return result;
  }

  @override
  Future<bool> acceptChallenge(String id) async => true;

  @override
  Future<bool> finishChallenge(String challengeId) async => true;
}
