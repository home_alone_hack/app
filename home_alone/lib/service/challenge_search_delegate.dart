import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_alone/dependency_injection/locator.dart';
import 'package:home_alone/store/challenge_store.dart';
import 'package:home_alone/view/widgets/challenge/discover_challenge_tile.dart';
import 'package:home_alone/view/widgets/themed_text.dart';
import 'package:home_alone/view/widgets/challenge/challenge_icon.dart';
import 'package:home_alone/viewmodel/challenge_model.dart';

class ChallengeSearchDelegate extends SearchDelegate {
  final searchResultsKey = new PageStorageKey('myListView');
  final ScrollController controller = ScrollController();

  ChallengeSearchDelegate({
    searchFieldLabel,
    keyboardType,
    textInputAction = TextInputAction.search,
  }) : super(
            searchFieldLabel: searchFieldLabel,
            keyboardType: keyboardType,
            textInputAction: textInputAction);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    return Theme.of(context).copyWith(
        textTheme: TextTheme(
            title: TextStyle(
                color: Colors.white, decoration: TextDecoration.none)),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: Theme.of(context)
              .textTheme
              .title
              .copyWith(color: Colors.white, fontSize: 15),
        ));
  }

  @override
  Widget buildResults(BuildContext context) {
    final challenges = locator.get<ChallengeStore>().findChallenges(query);
    return challenges.isEmpty ? _renderNoResults() : _renderResults(challenges);
  }

  Widget _renderResults(List<ChallengeModel> challenges) {
    return ListView.builder(
        itemCount: challenges.length,
        itemBuilder: (BuildContext context, int index) {
          final challenge = challenges[index];
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      child: CategoryIcon(
                        maxRadius: 16,
                        category: challenge.challenge.category,
                      ),
                      padding: EdgeInsets.only(left: 14.0),
                    ),
                    Padding(
                      child: ThemedText(
                        fontSize: 16,
                        text: challenge.challenge.category.name,
                        textAlign: TextAlign.left,
                      ),
                      padding: EdgeInsets.only(left: 8),
                    )
                  ],
                ),
              ),
              DiscoverChallengeTile(ChallengeModel(challenge.challenge), () {
                locator
                    .get<ChallengeStore>()
                    .acceptChallenge(challenge.challenge.id);
              })
            ],
          );
        });
  }

  Container _renderNoResults() {
    return Container(child: Center(child: Text("Keine Challenges gefunden")));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return buildResults(context);
  }
}
